package it.accaemme.sgame;


import java.util.ArrayList;
import java.util.Random;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Vibrator;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.ads.AdRequest;
import com.google.ads.AdSize;
import com.google.ads.AdView;


public class SGame extends Activity {
	private TextView text1;
	private int i;
	private static ArrayList<String> obblighi = new ArrayList<String>();
	private static ArrayList<String> veritas = new ArrayList<String>();
	public int max_players;
	private int cur_player;

	/*
	 * https://it.admob.com/my_sites/?_cd=1 <--- pubblicità
	 */
	
	
	private void setMax(int m){
		System.out.println("**setMax: "+m);
		if(m<2) 
			this.max_players=2;
		else
			this.max_players = m;
		System.out.println("**setMax this.max_players "+this.max_players);
	}

	private String getAzione(){
		ArrayList<String> action = new ArrayList<String>();
		action.add("bacia");
		action.add("mordi");
		action.add("solletica");
		action.add("pizzica");
		action.add("lecca");
		action.add("odora");
		action.add("sfiora");
		action.add("accarezza");
		action.add("massaggia");
		action.add("succhia");
		action.add("soffia");
		action.add("agita");
		action.add("stringi");
		
		Random rand = new Random();
		int m = rand.nextInt(action.size());
		
		return action.get(m);
	}
	
	private String getVestiti(){
		ArrayList<String> vestiti = new ArrayList<String>();
		vestiti.add("maglia");
		vestiti.add("scarpe");
		vestiti.add("calzini");
		vestiti.add("pantalone");
		vestiti.add("cannottiera");
		vestiti.add("cappello");
		vestiti.add("occhiali");
		vestiti.add("Braccialetti, orecchini, anelli");
		
		Random rand = new Random();
		int m = rand.nextInt(vestiti.size());
		
		return vestiti.get(m);
	}
	
	private String getBodyPiece(int livello){
		ArrayList<String> body_parts = new ArrayList<String>();
		if(livello < 0) livello = 0;
		
		if(livello == 0 || livello == 3){
			body_parts.add("testa");
			body_parts.add("guancia");
			body_parts.add("orecchie");
			body_parts.add("occhio");
			body_parts.add("naso");
			body_parts.add("labbra");
			body_parts.add("denti");
			body_parts.add("lingua");
			body_parts.add("collo");
			body_parts.add("baffi");
			body_parts.add("capelli");
			body_parts.add("spalla");
			body_parts.add("braccio");
			body_parts.add("ascelle");
			body_parts.add("ombelico");
			body_parts.add("fianchi");
			body_parts.add("vita");
			body_parts.add("schiena");
			body_parts.add("piede");
			body_parts.add("bocca");
			body_parts.add("unghia piede");
		} else if(livello == 1 || livello == 3){
			body_parts.add("inguine");
			body_parts.add("capezzolo");
			body_parts.add("zona intima");
			body_parts.add("interno coscia");
		} else if(livello == 2 || livello == 3){
			body_parts.add("pene/clitoride");
			body_parts.add("scroto/labbra vaginali");
		}
		body_parts.add("petto");
		body_parts.add("sedere");
		
		Random rand = new Random();
		int m = rand.nextInt(body_parts.size());
		
		return body_parts.get(m);
	}
	
	private int getMaxPlayers(){
		return (this.max_players);
	}
	
	private int getPlayer(int max){
		Random rand = new Random();
		int m = max;
		if(max == 0) m=2; 
		return (rand.nextInt(m))+1;
	}
	
	private String getDxOrSx(){
		Random rand = new Random();
		int m = rand.nextInt(4);
		return ((m%2)==0)?"destra":"sinistra";
	}
	
	private String getTruthorDare(){
		Random rand = new Random();
		int m = rand.nextInt(4);
		return ((m%2)==0)?"obbligo":"verità";
	}
	
	public static void frasi(int maxplayers, int livello){
		SGame s = new SGame();
		s.setMax(maxplayers);
		s.getMaxPlayers();

		// DEBUG
		// livello = 4;
		
		if(livello == 0 ){
			
		}
		if(livello == 0 || livello == 3){
			 obblighi.add("Salta su una gamba accarezzandoti la pancia e picchiandoti la testa con l'altra, nel frattempo devi dire uno scioglilingua");
			 obblighi.add("Fai il solletico ad un giocatore a tua scelta");
			 obblighi.add("Passa sotto le gambe di tutti i giocatori");
			 obblighi.add("Abbraccia i giocatori, uno ad uno");
		     obblighi.add("Mettiti in ginocchio e striscia in modo sexy");
		     obblighi.add("In 1 minuto, pensa ad una barzelletta sporca e raccontala");
		     obblighi.add("Se fossi un animale, quale saresti? Imitalo");
		     obblighi.add("Se i giocatori fossero animali, quali sarebbero?");
		     obblighi.add("Decidi cosa e a chi farlo fare (anche più giocatori)");
		     obblighi.add("Prendi un pezzo di frutta e mangialo in maniera molto sensuale");
		     obblighi.add("Scrivi un messaggio intrigante sullo specchio del bagno che io possa leggere più tardi");
			 obblighi.add("Cucina qualcosa in maniera sensuale");
		     obblighi.add("Fai 1 minuto di stretching");
		     obblighi.add("Fai un ballo osè");
		     obblighi.add("Usando le dita, scrivi un messaggio sulla schiena del giocatore n° " + s.getPlayer(maxplayers) + ". Se il giocatore non capisce, deve cliccare su "+s.getTruthorDare());
		     
		     obblighi.add("Fai un massaggio ai piedi del giocatore "+String.valueOf(s.getPlayer(maxplayers)));
			 obblighi.add("Dì il nome del giocatore n° "+String.valueOf(s.getPlayer(maxplayers))+" in 3 modi diversi, in modo sempre più provocante");
			 obblighi.add("Per ogni lettera del nome del giocatore n° "+String.valueOf(s.getPlayer(maxplayers))+" , dì qualcosa di sensuale che lo/la riguarda");
			 obblighi.add("Sussurra qualcosa di speciale in una lingua straniera al giocatore n° "  + String.valueOf(s.getPlayer(maxplayers)) );
		
			 // BodyParts + n° giocatore
			 obblighi.add("Siediti per 2 minuti sulle gambe del giocatore n° "  + String.valueOf(s.getPlayer(maxplayers)) );
		}
		 if(livello == 1 || livello == 3){
			 obblighi.add("Indossa la maglia al contrario");
			 obblighi.add("Indossa il pantalone al contrario");
			 obblighi.add("Indossa la maglia al posto del pantalone e viceversa.");
			 obblighi.add("Cala i pantaloni e canta o' sole mio");
			 obblighi.add("Resta in intimo e siediti su ogni giocatore");
		     obblighi.add("Resta in intimo e gira attorno a tutti i giocatori");
		     obblighi.add("Con il solo intimo, atteggiati come se dovessi fare degli scatti per un calendario sexy");
		     obblighi.add("Proponi un gioco di scambio di ruolo");
		     obblighi.add("Rimani in intimo e mettiti in pose sexy per 2 minuti");
		     obblighi.add("Fai un ballo sexy per 1 minuto");
		     obblighi.add("Alza la maglietta e fai una sexy danza del ventre");
		     obblighi.add("Sdraiati sulla schiena in intimo e alza le gambe verso l'alto 10 volte");
		     obblighi.add("Sderaiati sul letto e mettiti nella posa più sexy che conosci");
		     obblighi.add("Decidi cosa e a chi farlo fare (anche più giocatori)");
		     obblighi.add("Scrivi una breve frase erotica. (su carta o cellulare)");
		     obblighi.add("Pensa a una nuova posizione sessuale e mostrala, tenendo però addosso i vestiti");
		     obblighi.add("Imita una posizione erotica difficile");

		     obblighi.add("Fai finta che il giocatore n° "+ String.valueOf(s.getPlayer(maxplayers)) +" sè uno/a straniero/a che incontri in un negozio di libri. Devi convincerlo/a a venire a casa con te");
			 obblighi.add("Passa una caramella dura dalla bocca del giocatore n° "  + String.valueOf(s.getPlayer(maxplayers)) + " alla tua bocca per 3 minuti");
			 obblighi.add("Scrivi una breve frase erotica per il giocatore n° "  + String.valueOf(s.getPlayer(maxplayers)) );
			 obblighi.add("Disegnando nell'aria la silhouette del giocatore, segui le forme del corpo dai piedi alla testa senza del giocatore n° "  + String.valueOf(s.getPlayer(maxplayers)) );
			 obblighi.add("Bevi dell'acqua calda o del the e poi bacia il collo per 3 minuti il giocatore n° "  + String.valueOf(s.getPlayer(maxplayers)) );
		 	 obblighi.add("Tira peli, che non siano capelli(o peli del viso) al giocatore n° "  + String.valueOf(s.getPlayer(maxplayers)) );
			 obblighi.add("Bacia giocatore n° " + String.valueOf(s.getPlayer(maxplayers)) +" su "+ s.getBodyPiece(livello) );
			 obblighi.add("Odora il giocatore n° "+ String.valueOf(s.getPlayer(maxplayers))+" partendo dal collo, spalle, fianchi, interno coscia e infine su " + s.getBodyPiece(livello));
			 obblighi.add("Dai uno schiaffo al giocatore alla tua "+s.getDxOrSx() +" su "+ s.getBodyPiece(livello)  );
			 
				// BodyParts + n° giocatore
			 obblighi.add("Per i prossimi 2 minuti, il giocatore n° "  + String.valueOf(s.getPlayer(maxplayers)) +" è il padrone e tu devi fare ciò che dice" );
			 
			 obblighi.add("Sgancia una puzza accanto al giocatore n° " + String.valueOf(s.getPlayer(maxplayers)) );
		 }
		 if(livello == 2 || livello == 3){
					 obblighi.add("Spogliati di tutti i vestiti e corri velocemente davanti alla finestra");
					 obblighi.add("SUPER HOT: Lecca perineo di un giocatore.");
				     obblighi.add("SUPER HOT: Lecca clitoride di un giocatore.");
				     
				  // Vibratore	  
						obblighi.add("Usa vibratore");
			
				ArrayList<String> kamasutra2 = new ArrayList<String>();
				kamasutra2.add("#La sexy V#\nLa donna deve sedersi su un mobile o un tavolo e l'uomo deve mettersi di fronte a lei in piedi. Le gambe di lui devono essere leggermente piegate, distanziate di circa 90 cm. La donna appoggia le braccia dell'uomo, che ha invece le braccia attorno alla parte inferiore del busto di lei. Lentamente la donna deve spingere la gamba sinistra in alto, e sostenere il piede destro sulla spalla sinistra dell'uomo. Fare la stessa cosa con la gamba destra sulla spalla sinistra dell'uomo.");
				kamasutra2.add("#Cavallo a dondolo#\nL'uomo siede a gambe incrociate, tenendosi con le mani poggiate dietro. La donna si siede sull'uomo, con il viso rivolto verso di lui, avvolgendolo con le gambe. La donna può così decidere il ritmo e la profondità della penetrazione. Per liberare le braccia e accarezzare la donna, l'uomo potrebbe poggiare la schiena contro una parete, per avere così le mani libere.");
				obblighi.add( "Esegui posizione del kamasutra su un giocatore a tua scelta" + kamasutra2.get( (int) (Math.random()*kamasutra2.size()) ) );
				/*
				 ***
				 Prendendoli da: http://www.alfemminile.com/coppia/album856235/le-100-migliori-posizioni-del-kamasutra-0.html#p46
				 mettere anche le immagini come ImageView
				 ***
				 */
				 		
				
				
				 // BodyParts + n° giocatore
						obblighi.add("Togli un capo di abbigliamento usando solo la tua bocca al giocatore n° "  + String.valueOf(s.getPlayer(maxplayers)) );
						obblighi.add("Tocca con le mani, una in un posto diverso dall'altra, due zone intime del giocatore n° " + String.valueOf(s.getPlayer(maxplayers)) );
						
						
						veritas.add("Dimmi, cosa hai pensato la prima volta che mi hai visto nudo/a? Se non è mai successo, obbligo: Spogliati seminudo/a. Parte di sopra completamente nuda.");
		 }
		 if(livello == 1 || livello == 2 || livello == 3){
			// Vestiti
			 obblighi.add("Togli "+s.getVestiti()+" al giocatore "+s.getPlayer(maxplayers));
			 obblighi.add("Togli "+s.getVestiti()+" al giocatore "+s.getPlayer(maxplayers));
			 obblighi.add("Togli "+s.getVestiti()+" al giocatore "+s.getPlayer(maxplayers));
			 
			 // valore compreso tra 0 e 10
			 obblighi.add("Togli "+String.valueOf( ( (int)(Math.random()*10) ) )+" capi (vestiti)");
			 obblighi.add("Togli "+String.valueOf( ( (int)(Math.random()*10) ) )+" capi (vestiti)");
			 obblighi.add("Togli "+String.valueOf( ( (int)(Math.random()*10) ) )+" capi (vestiti)");

		    // Azioni
			 obblighi.add( " [" + s.getAzione() + "] - [" + s.getBodyPiece(livello) + "] giocatore n° " + s.getPlayer(maxplayers) );
			 obblighi.add( " [" + s.getAzione() + "] - [" + s.getBodyPiece(livello) + "] giocatore alla tua " + s.getDxOrSx() );
			 obblighi.add( "Sfiora ["+ s.getBodyPiece(livello) +"] del giocatore n° " + s.getPlayer(maxplayers) +" con tuo [" + s.getBodyPiece((livello-1)+1) + "]" );
					
		 }
		 
		 if(livello==4){ // DEBUG MODE
			 // Modalità per testare obblighi e verità
			 
			 obblighi.add("DEBUG");
			 veritas.add("DEBUG");
		 } else {
	
		/* # Ce la fai? # */
		String celafai_start = "# Ce la fai? #\n";
		obblighi.add(celafai_start+"Fai l'occhiolino con l'occhio destro e l'occhio sinistro, in modo alternato, "+String.valueOf( ( (int)(Math.random()*10) ) )+" volte.\n Se non ci riesci clicca "+String.valueOf( ( (int)(Math.random()*10) ) )+" volte su "+s.getTruthorDare());
		obblighi.add(celafai_start+"Ripeti velocemente: ~ Mazzo di carte, carte di mazzo~ "+String.valueOf( ( (int)(Math.random()*10) ) )+" volte.\n Se sbagli clicca "+String.valueOf( ( (int)(Math.random()*10) ) )+" volte su "+s.getTruthorDare());
		obblighi.add(celafai_start+"Ripeti velocemente: ~ Sopra la panca la capra campa, sotto la panca la capra crepa~ "+String.valueOf( ( (int)(Math.random()*10) ) )+" volte.\n Se sbagli clicca "+String.valueOf( ( (int)(Math.random()*10) ) )+" volte su "+s.getTruthorDare());
		obblighi.add(celafai_start+"Senza piegare le gambe, toccare con le mani le punte dei piedi, "+String.valueOf( ( (int)(Math.random()*10) ) )+" volte.\n Se non ci riesci clicca "+String.valueOf( ( (int)(Math.random()*10) ) )+" volte su "+s.getTruthorDare());
		obblighi.add(celafai_start+"Dì i nomi di almeno , "+String.valueOf( ( (int)(Math.random()*10) ) )+" personaggi di futurama.\n Per tutti quelli non detti, clicca "+String.valueOf( ( (int)(Math.random()*10) ) )+" volte su "+s.getTruthorDare());
		obblighi.add(celafai_start+"Dì i nomi delle 7 renne di Babbo Natale.\n Per tutti quelli non detti, clicca su "+s.getTruthorDare());
		obblighi.add(celafai_start+"Ripeti velocemente "+String.valueOf( ( (int)(Math.random()*10) ) )+" volte: VUOI QUEI KIWI?.\n Per tutti quelli non detti, clicca su "+s.getTruthorDare());
		
		
		/* # Kamasutra # */
		 ArrayList<String> kamasutra = new ArrayList<String>();
		 kamasutra.add("69"); kamasutra.add("missionario"); 
		 kamasutra.add("farfalla");  kamasutra.add("");
		 //obblighi.add(kama + kamasutra.get(( (int) (Math.random()*kamasutra.size()) )) + " con giocatore n°" + String.valueOf(s.getPlayer(maxplayers)) +"\n Se non ci riesci clicca "+String.valueOf( ( (int)(Math.random()*10) ) )+" volte su "+s.getTruthorDare());
		 obblighi.add("Esegui posizione del kamasutra: \""+ kamasutra.get( (int) (Math.random()*kamasutra.size()) ) + "\" con giocatore n° " + String.valueOf(s.getPlayer(maxplayers)) + ". Chi non riesce o non è capace, toglie un capo(vestito)" );
		 
		 
			 obblighi.add("Salta il turno");
			 obblighi.add("Clicca verità");
			 obblighi.add("Decidi cosa e a chi farlo fare (anche più giocatori)");     	
			 obblighi.add("Bacia il giocatore alla tua "+s.getDxOrSx()+" su "+ s.getBodyPiece(livello) );
			 	  
			 	  	
			 veritas.add("Preferisci baciare su una pista da ballo affollato o di una sala buia e tranquilla?");
			 veritas.add("Qual è l'uomo politico che ritieni più sexy?");
			 veritas.add("Qual è l'unica cosa che non vorresti provare a letto?");
			 veritas.add("Quando è stata la tua prima volta?");
			 veritas.add("Preferiresti legare con una corda o leccare il gelato spalmato sul corpo?");
			 veritas.add("Quando è l'ultima volta che sei uscito/a senza biancheria intima?");
			 veritas.add("Hai mai avuto orgasmi multipli? Se sì, quando e con chi?");
			 veritas.add("Dimmi se ti è mai venuto in mente di fermare un giocatore del gruppo, baciarlo e fare l'amore con lui/lei all'improvviso. Se sì, quando?");
			 veritas.add("Di' qualcosa di sporco che diresti a letto");
			 veritas.add("Hai mai provato a fare l'amore in un bagno pubblico?");
			 veritas.add("Ti sei mai eccitato/a sul lavoro/scuola? Descrivimi quel momento");
			 veritas.add("Cos'è più eccitante? Contro la parete o sul tavolo?");
			 veritas.add("Fingi di essere un critico cinematografico e che hai appena visto un film in cui vedi te e il giocatore n° "+String.valueOf(s.getPlayer(maxplayers))+" che fate l'amore. Dai un giudizio critico su questo film, esplicando passo dopo passo il giudizio su ogni scena.");
			 veritas.add("Quale tipo di lingerie ritieni doni di più al giocatore n° "+String.valueOf(s.getPlayer(maxplayers))+"?");
			 veritas.add("Quale abito indosso al giocatore n° "+String.valueOf(s.getPlayer(maxplayers))+" ti farebbe eccitare?");
			 veritas.add("Di' un aneddoto divertente sul fare l'amore");
			 veritas.add("Descrivi una sessione di sesso di un'ora tra te e il giocatore n° "+String.valueOf(s.getPlayer(maxplayers))+" . Che cosa accadrebbe?");
			 veritas.add("Quale tipo di lingerie ritieni che mi doni di più al giocatore n° "+ String.valueOf(s.getPlayer(maxplayers)));
			 veritas.add("Dimmi qual è la tua zona erogena su cui vorresti che ci si soffermasse di più");
			 veritas.add("Qual è il posto più folle in cui ti sei masturbato/a");
			 veritas.add("Qual è il luogo più bizzarro in cui vorresti fare l'amore?");
			 veritas.add("Descrivi una scena di un film che ti eccita davvero e specifica quale film è");
			 veritas.add("Preferiresti farlo sul cofano di un'auto o sulla lavatrice?");
			 veritas.add("C'è qualcosa di sporco che avresti sempre voluto dire a letto?");
			 veritas.add("Pensi che il giocatore n° "+String.valueOf(s.getPlayer(maxplayers))+" sia più sexy in costume da bagno o in accappatoio?");
			 veritas.add("Qual è la posizione sessuale che vorresti provare con il giocatore n° "+String.valueOf(s.getPlayer(maxplayers))+"? Mimala sul suo corpo");
			 veritas.add("Se avessi un intero giorno da trascorrere a letto con il giocatore n° "+ String.valueOf(s.getPlayer(maxplayers)) +", cosa vorresti fare?");
			 veritas.add("Preferisci provare un rapporto a tre o girare un video di noi mentre fai l'amore?");
			 veritas.add("Se dovessi girare una scena in un film porno, chi sarebbe la co-protagonista?");
			 veritas.add("Chi è stata la prima persona che hai baciato? Ti è piaciuto?");
			 veritas.add("Quale canzone ti piace/piacerebbe di più per fare l'amore?");
			 veritas.add("Hai mai fatto il bagno completamente nudo/a? Quando e dove?");
			 veritas.add("Se potessi trascorrere una notte di sesso con un personaggio famoso, chi sarebbe?");
			 veritas.add("Qual è il tuo momento di eccitazione più imbarazzante?");
			 veritas.add("Dopo l'amore, preferisci coccole o fare la doccia insieme?");
			 veritas.add("Pensi che il sesso trasgressivo sia meglio del sesso normale?");
			 veritas.add("Descrivi quello che per te è il bacio perfetto");
			 veritas.add("Descrivi come ti prepari prima di un appuntamento");
			 veritas.add("Quale posto ritieni sia il luogo più sexy del mondo?");
			 veritas.add("Quale cibo ti fa impazzire?");
			 veritas.add("Preferisci fare l'amore sulla spiaggia o in una foresta?");
			 veritas.add("Durante l'amore cosa preferisci, candele o musica?");
			 veritas.add("Descrivi una sessione di sesso di un'ora tra te e il giocatore " + String.valueOf(s.getPlayer(maxplayers)) + ". Cosa accadrebbe?");
			 veritas.add("Qual è la tua tipologia preferita di scena di sesso presa da un film?");
			 veritas.add("Dimmi un luogo pubblico in cui vorresti davvero fare sesso");
			 veritas.add("Qual è la zona più delicata del tuo corpo?");
			 veritas.add("Qual è il punto più erotico dove potrei fare un piercing?");
			 veritas.add("Hai mai tradito qualcuno?");
			 veritas.add("Quando è stata la tua prima volta?");
			 veritas.add("Sei mai stato innamorato?");
			 veritas.add("Se trovassi una bacchetta magica quale sarebbe il tuo desiderio?");
			 veritas.add("Se dovessi scegliere di farlo con una persona famosa, chi sarebbe?");
			 veritas.add("Se dovessi scegliere di farlo con qualcuno che conosci, chi sarebbe?");
			 veritas.add("Se dovessi scegliere di fare per forza qualche azione sessuale con qualcuno del gruppo con cui stai giocando, cosa faresti e con chi?");
			 veritas.add("Racconta l'esperienza piu imbarazzante che hai vissuto");
			 veritas.add("Sei mai stata/o invidioso di qualcuno? Se sì, di chi?");
			 veritas.add("A che età hai dato il tuo primo bacio?");
			 veritas.add("Ti sei mai masturbato/a con l'altra mano?");
			 veritas.add("Ti sei toccato/a durante l'ultima doccia che hai fatto?");
			 veritas.add("Faresti mai qualcosa con un/a amico/a? Lo hai mai fatto? Ci hai mai pensato? Desidereresti farlo? Con chi?");
			 veritas.add("Confessa la cosa più intima che hai fatto, da solo, con il tuo corpo");
			 veritas.add("Hai mai provato a guardare un tuo buco( anale e/o vaginale ) allo specchio?");
			 
			 /* Ridondanti */
			 obblighi.add( " [" + s.getAzione() + "] - [" + s.getBodyPiece(livello) + "] giocatore n° " + s.getPlayer(maxplayers) );
			 obblighi.add( " [" + s.getAzione() + "] - [" + s.getBodyPiece(livello) + "] giocatore alla tua " + s.getDxOrSx() );
			 veritas.add("Clicca obbligo");
			 veritas.add("Clicca obbligo");
			 veritas.add("Clicca obbligo");
			 veritas.add("Salta il turno");
			 veritas.add("Clicca obbligo");
		 }
	}
	
	public void clearArray(){
		obblighi.clear();
		veritas.clear();
	}
	
	public int getCurPlayer(){
		int cur = this.cur_player;
		return cur;
	}
	
	public void setCurPlayer(int cur){
		this.cur_player = cur;
	}		
	
	public void nextPlayer(int maxplayers){
		int cur = this.cur_player;
		
		if(( cur >= maxplayers) || (cur <= 0)){				
			cur = 1;
		} else {
			cur = cur+1;
		}
		
		this.setCurPlayer(cur);
	}

   
	public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
      
     
        text1 = (TextView) findViewById(R.id.text1);
        text1.setTextColor(Color.BLUE);
        text1.setTextSize(30);
        
        String pkg=getPackageName(); 
        Intent intent = getIntent(); // l'intent di questa activity
        final int nplayers = intent.getIntExtra(pkg+".giocatori", -1);
        final int livello = intent.getIntExtra(pkg+".livello", -1);

        
        Button obbligo = (Button) findViewById(R.id.obbligo);
        Button verita = (Button) findViewById(R.id.verita);
        final Button vibra = (Button) findViewById(R.id.button_vibratore);
        //final EditText edit1 = new EditText(this);
        final TextView sottofrase = (TextView) findViewById(R.id.sottofrase);
        final Vibrator vibratore = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);;

        TextView tv_livello = (TextView) findViewById(R.id.livello);
        if(livello==0) tv_livello.setText("Livello ("+livello+"): EASY");
        if(livello==1) tv_livello.setText("Livello ("+livello+"): MIDDLE");
        if(livello==2) tv_livello.setText("Livello ("+livello+"): HARD");
        if(livello==3) tv_livello.setText("Livello ("+livello+"): MISTO");
        if(livello==4) tv_livello.setText("Livello ("+livello+"): MODALITA' DEBUG");
        
        setCurPlayer(1);
        
        obbligo.setOnClickListener(new OnClickListener(){
			public void onClick(View arg0){
				frasi(nplayers, livello); // Carico l'arraylist di frasi, in base al livello configurato.
        		Random rand = new Random();
       		 	i = rand.nextInt(obblighi.size());
        		
       		    
       		 TextView turno_giocatore = (TextView) findViewById(R.id.turno_giocatore);
       		 turno_giocatore.setText("[- È il turno del giocatore n° " + String.valueOf(getCurPlayer()) + " su " + nplayers + " -]");
       		 	
        		TextView frase = (TextView) findViewById(R.id.frase);
                frase.setText(obblighi.get(i));
        		
        		if( ((String) frase.getText()).equalsIgnoreCase("Usa vibratore") ){ 
        			vibra.setVisibility(1);
        			sottofrase.setVisibility(1);
        			int tempo = rand.nextInt(59)+1;
        			sottofrase.setText("per "+String.valueOf(tempo)+" secondi.");
        	        final int tempovibra = tempo*1000; // ms * 1000 = 1 secondo. 50 secondi,1 min
        	        
        	        vibra.setOnClickListener(new View.OnClickListener(){
        				@Override
						public void onClick(View v) {
        			       	vibratore.vibrate(tempovibra);
        			       	sottofrase.setVisibility(-1);
        			       	vibra.setVisibility(-1);
        				}
        			});
        		}
        		
                //frase.setText(String.valueOf(y.size()));
        		
                clearArray();
               nextPlayer(nplayers);
        	}
        });
        
        verita.setOnClickListener(new OnClickListener(){
        	public void onClick(View arg0){
        		frasi(nplayers,livello);
        		Random rand = new Random();
       		 	i = rand.nextInt(veritas.size());
        		
       		 	TextView turno_giocatore = (TextView) findViewById(R.id.turno_giocatore);
       		 	turno_giocatore.setText("È il turno del giocatore n° " + String.valueOf(getCurPlayer()) + " su " + nplayers );
       		 	
        		TextView frase = (TextView) findViewById(R.id.frase);
                frase.setText(veritas.get(i));
        		
        		
                //frase.setText(String.valueOf(y.size()));
                clearArray();
                nextPlayer(nplayers);
        	}
        });


        String id_editore= "a150c7a234cea5a";
        AdView adView = new AdView(this, AdSize.BANNER, id_editore);
        LinearLayout layout = (LinearLayout) this.findViewById(R.id.SGame);
        layout.addView(adView);
        adView.loadAd(new AdRequest());

	 }
}