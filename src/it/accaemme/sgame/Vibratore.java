package it.accaemme.sgame;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.os.Vibrator;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class Vibratore extends Activity{
    
    public void onCreate(Bundle savedInstanceState) {
        
        int default_value = 50*1000; // ms * 1000 = 1 secondo. 50 secondi,1 min
        
        // final EditText edit1 = (EditText) findViewById(R.id.editText1);
        final EditText edit1 = new EditText(this);
        final Vibrator vibratore = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
        // edit1.setHint(default_value);
        
        // Button button1 = (Button) findViewById(R.id.button);
        Button button1 = new Button(this);
        button1.setOnClickListener(new View.OnClickListener(){
			public void onClick(View v) {
				int valore = Integer.valueOf( (edit1.getText()).toString() );
		       	vibratore.vibrate(valore);
			}
		});
        
        
    }
}