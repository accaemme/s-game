package it.accaemme.sgame;

/**/

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.SeekBar;
import android.widget.SeekBar.OnSeekBarChangeListener;
import android.widget.TextView;

public class Intro extends Activity {
    /** Called when the activity is first created. */
	
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.intro);

    	final TextView tw_livello = (TextView) this.findViewById(R.id.textViewProgress);
        
        final EditText nplayers = (EditText)this.findViewById(R.id.nPlayers);
        nplayers.setHint("2");
        
        final SeekBar level = (SeekBar) findViewById(R.id.seekBar1);
        OnSeekBarChangeListener MySeekBarListener = new OnSeekBarChangeListener() {
            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                    tw_livello.setText("Livello selezionato: " + String.valueOf(level.getProgress()));
                    //this_level = level.getProgress();
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
            		tw_livello.setText("Scorri la barra e seleziona li livello di gioco");
            }

            @Override
            public void onProgressChanged(SeekBar seekBark, int progress, boolean fromUser) {
            	tw_livello.setText("Livello: " + String.valueOf(progress));
            }
         };
         level.setOnSeekBarChangeListener(MySeekBarListener);


        
        
        ImageButton button = (ImageButton) findViewById(R.id.imageButton1);
        
        
        button.setOnClickListener(new OnClickListener(){
        	public void onClick(View arg0){
        		int n = 2;
                try{
                	//if(temp.matches("/^\d+$/"))
                		n = Integer.valueOf(nplayers.getText().toString());
                }
                catch (NumberFormatException nfe) {
                	  System.out.println("Could not parse " + nfe);
        		}	
        		
        		
                	Intent entra = new Intent(Intro.this, SGame.class);
                	String pkg=getPackageName(); 
                	entra.putExtra(pkg+".giocatori", n);
                	entra.putExtra(pkg+".livello", level.getProgress());
                	Intro.this.startActivity(entra);
                }
        });
    
    }
}